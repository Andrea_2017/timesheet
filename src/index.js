
import React from 'react';
import ReactDOM from 'react-dom';


import Timesheet from './lib/components/Timesheet'

import $ from 'jquery';
import momentit from 'moment/locale/it';

import 'bootstrap/dist/css/bootstrap.css';
import 'myCss/myStyle.css';
import 'react-table/react-table.css';

let data = {
        resource: {
                id: 123,
                fullName: "Rossi Paolo"
        }
        , options: {}
        , year: 2018

};

const app = <div> <Timesheet data={data} /> </div>

ReactDOM.render(app, document.getElementById('app'));