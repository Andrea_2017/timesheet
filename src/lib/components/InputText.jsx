import BaseComponent from './BaseComponent'

const { moment, React } = BaseComponent.BaseClass;

export default class InputText extends BaseComponent {
  constructor(props) {
    super(props);
    this.handleChangeInputText = this.handleChangeInputText.bind(this);
    this.state = {};
  }

  handleChangeInputText(event) {
    const target = event.target;
    this.setState({oraIngresso: target.value});
    console.log('inserito il valore: ' + target.value);
  }

  render() {
    return (
      <input type="text" className="inputTextWidht90" defaultValue={this.props.valore} 
        name={this.props.nome} onChange={this.handleChangeInputText} />
    );
  }
}