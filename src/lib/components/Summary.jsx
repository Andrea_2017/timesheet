import BaseComponent from './BaseComponent'

const { React } = BaseComponent.BaseClass;
const { Component } = React;

function LabelSummary(props) {
  return <div className="divStyle">{props.label}</div>
}

function ValueSummary(props) {
  return <div className="divStyle witdhStyle30PX">{props.value} </div>
}

export default class Summary extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = {
      totaleOreLavorateMese: props.totaleOreLavorateMese || 0
      , giorniLavorati: props.giorniLavorati || 0
      , oreStraordinario: props.oreStraordinario || 0
    };
  }
  render() {
    let widthStyle = { width: '40px' };
    return (
      <div>
        <div>
          <LabelSummary label={"TOT hh Lavorate: "} />
          <ValueSummary value={this.props.totaleOreLavorateMese} />
        </div>
        <div className="divStyle">
          <LabelSummary label={"GG Lavorati: "} />
          <ValueSummary value={this.props.giorniLavorati} />
        </div>
        <div className="divStyle">
          <LabelSummary label={"Ore Extra Infrasett: "} />
          <ValueSummary value={this.props.oreStraordinarioInfrassettimanali} />
        </div>
        <div className="divStyle">
          <LabelSummary label={"hh Extra WE: "} />
          <ValueSummary value={this.props.oreStraordinarioFineSettimana} />
        </div>
        <div className="divStyle">
          <LabelSummary label={"Totale hh Straordinari: "} />
          <ValueSummary value={this.props.totaleStraordinari} />
        </div>
      </div>
    );
  }
}