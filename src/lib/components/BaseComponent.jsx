import BaseClass from '../BaseClass'

const { React, ReactDOM, $ } = BaseClass;

const { Component } = React;

export default class BaseComponent extends Component {
  constructor(props) {
    super(props);
  }
  static get BaseClass() {
    return BaseClass;
  }
  get $el() {
    return $(this.el);
  }

  componentDidMount() {
    //this.$el.somePlugin();
  }
  componentWillUnmount() {
    //this.$el.somePlugin('destroy');
  }
}