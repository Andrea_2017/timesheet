import BaseComponent from './BaseComponent'

const { React } = BaseComponent.BaseClass;
const { Component } = React;

export default class NoteSelector extends BaseComponent {
  constructor(props) {
    super(props);
    this.state = { value: '' }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
    console.log('hai cambiato la nota in: ' + event.target.value);
  }

  handleSubmit(event) {
    alert('Hai selezionato la nota: ' + event.target.value);
  }

  render() {
    return (
      <select value={this.state.value} onChange={this.handleChange} >
        <option value={""}>{""}</option>
        <option value={"P"}>{"FERIE"}</option>
        <option value={"P2"}>{"1/2 PERMESSO"}</option>
        <option value={"M"}>{"MALATTIA"}</option>
        <option value={"F"}>{"FESTIVO"}</option>
        <option value={"FN"}>{"FESTA NAZIONALE"}</option>
      </select>
    );
  }
}