import BaseComponent from './BaseComponent'

const { React } = BaseComponent.BaseClass;
const { Component } = React;

export default class CodSelector extends BaseComponent {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
    console.log('hai cambiato il codice in: ' + event.target.value);
  }

  render() {
    return (
      <select  onChange={this.handleChange}>
        <option value={""}>{""}</option>
        <option value={"TR"}>{"TRASFERTA"}</option>
        <option value={"SD"}>{"SEDE DISAGIATA"}</option>
      </select>
    );
  }
}