import BaseComponent from './BaseComponent'

const { moment, React, ReactDOM } = BaseComponent.BaseClass;
const { Component } = React;

import MonthSelector from './MonthSelector'
import Summary from './Summary'
import NoteSelector from './NoteSelector'
import CodSelector from './CodSelector'
import InputText from './InputText'

const columns = [
  { title: "", fieldName: "dayName" },
  { title: "Giorno", fieldName: "day" },
  { title: "Entrata", fieldName: "start" },
  { title: "Uscita", fieldName: "end" },
  { title: "Ore", fieldName: "worked" },
  { title: "PICAP", fieldName: "pIcap" },
  { title: "Reperib.", fieldName: "reperibilita" },
  { title: "Note (**)", fieldName: "note" },
  { title: "Trasferta", fieldName: "trasferta" },
  { title: "Cod(***)", fieldName: "cod" },
  { title: "Commessa", fieldName: "codCommessa" }
];

export default class Timesheet extends Component {
  constructor(props) {
    super(props);
    this.monthChanged = this.monthChanged.bind(this);
    this.handleChangeInputText = this.handleChangeInputText.bind(this);
    this.handlSubmit = this.handlSubmit.bind(this);
    this.state = {
      data: props.data || {},
      commessaValue: ''
    };
  }

  handleChangeCommessa(event) {
    console.log('hai cambiato la commessa in: ' + event.target.value);
    //this.setState({commessaValue: event.target.value});
  }

  handleChangeInputText(event) {
    console.log('inserito il valore: ' + event.target.value);
    this.setState({})
  }

  handlSubmit(event) {
    console.log('SUBMIT: ' + event.target.value);
  }

  monthChanged(event) {
    let { data } = this.state;
    data.month = event.value;
    let _self = this;
    let { resource = {}, options = {}, year, month = -1, detailDays = [] } = data;
    let { id } = resource;
    let url = `data/${id}/${year}/${month}.json`;
    fetch(url).then(function (response) {
      if (response.ok) {
        return response.json();
      }
      throw new Error('Network response was not ok.');
    })
    .then(function (dataJson) {
      _self.setState({
        data: dataJson
      });
    })
    .catch(function (error) {
      let dataJson = {
        resource, options, year, month, detailDays: []
      };
      _self.setState({
        data: dataJson
      });
      console.error('There has been a problem with your fetch operation: ' + error.message);
    });
  }

  render() {

    let marginLeft280 = { marginLeft: "280px" };

    let { data, datanew, timestamp } = this.state;
    let { resource = {}, options = {}, year, month = -1, detailDays = [] } = data;

    let totaleOreLavorateMese = 0;
    let giorniLavorati = 0;
    let oreStraordinarioFineSettimana = 0;
    let oreStraordinarioInfrassettimanali = 0;
    let totaleStraordinari = 0;

    let detailDaysMap = detailDays.reduce(function (map, obj) {
      map[obj.day] = obj;
      return map;
    }, {});
    let values = [];
    let daysInMonth = 0;
    if (month > 0) {
      let mom = moment([year, month - 1]);
      daysInMonth = mom.daysInMonth();
    }

    for (let i = 0; i < daysInMonth; i++) {
      let key = "" + (i + 1);
      let value = detailDaysMap[key] || {
        day: i + 1
      };
      /* straordinari del fine settimana */
      if ((moment([year, month - 1, (i + 1)]).weekday() > 4) && value.start) {
        oreStraordinarioFineSettimana += (value.end - value.start - 1);
      }

      if (value.start != null || value.end != null) {
        value.worked = value.end - value.start - 1;
        totaleOreLavorateMese += value.worked;
        giorniLavorati++;
        /* straordinari infrasettimanali */
        if ((moment([year, month - 1, (i + 1)]).weekday() < 5) && value.worked > 8) {
          oreStraordinarioInfrassettimanali += (value.worked - 8);
        }
      }
      if (value.worked == 0) {
        value.worked = null;
      }
      value.pIcap = value.worked
      values.push(value);
    }
 
    totaleStraordinari = oreStraordinarioInfrassettimanali + oreStraordinarioFineSettimana;

    return (
      <div>
        <form onSubmit={this.handlSubmit} >
          <div className={"well"}>{resource.fullName}</div>
          <MonthSelector value={month} year={year} onChange={this.monthChanged} />

          <Summary totaleOreLavorateMese={totaleOreLavorateMese} giorniLavorati={giorniLavorati}
            oreStraordinarioInfrassettimanali={oreStraordinarioInfrassettimanali}
            oreStraordinarioFineSettimana={oreStraordinarioFineSettimana} totaleStraordinari={totaleStraordinari} />

          <table className="table table-bordered">
            <thead>
              <tr>
                {columns.map((item, key) => <td key={key}>{item.title}</td>)}
              </tr>
            </thead>
            <tbody>
              {values.map((itemValue, key) => {
                let mom = moment([year, month - 1, itemValue.day]);
                let weekDay = mom.weekday();
                let classes = "";
                itemValue.start = <InputText valore={itemValue.start} nome={"oraIngresso"} />
                itemValue.end = <InputText valore={itemValue.end} />
                itemValue.reperibilita = <InputText valore={itemValue.reperibilita} />
                itemValue.trasferta = <InputText valore={itemValue.trasferta} />
                itemValue.codCommessa = <InputText valore={itemValue.codCommessa} />

                if (weekDay >= 5) {
                  classes = "info"
                } else {
                  if (!itemValue.note) {
                    itemValue.note = <NoteSelector />
                  }
                  if (!itemValue.cod) {
                    itemValue.cod = < CodSelector />
                  }
                }
                return (
                  <tr className={classes} key={key}>
                    {columns.map((item, key) => {
                      let fn = item.fieldName;
                      let v = itemValue[item.fieldName];
                      if (fn == "dayName") {
                        v = mom.format("dddd");
                      }
                      else if (fn == "day") {
                        v = mom.format("DD");
                      }
                      return (<td key={key}>{v}</td>)
                    }
                    )}
                  </tr>
                );
              }
              )}
            </tbody>
          </table>
          <div className = "marginLeft280">
            <Summary totaleOreLavorateMese={totaleOreLavorateMese} giorniLavorati={giorniLavorati}
              oreStraordinarioInfrassettimanali={oreStraordinarioInfrassettimanali}
              oreStraordinarioFineSettimana={oreStraordinarioFineSettimana}
              totaleStraordinari={totaleStraordinari} />
          </div>
          <br />
          <input type="submit" value="Conferma Timeshet" />
        </form>
      </div>
    );
  }
}