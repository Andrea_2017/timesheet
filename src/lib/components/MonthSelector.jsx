import BaseComponent from './BaseComponent'

const {moment,React} = BaseComponent.BaseClass ;

class MonthOption extends BaseComponent {
  constructor(props) {
    super(props);
  }
  render() {
    let { key, value, name } = this.props.data;
    return (<option value={value}>{name}</option>);
  }
}

let months = null;
const getMonths = (year) => {
  if (months != null) {
    return months;
  }
  let result = moment.months();
  result = result.map((e, idx) => { return { name: e + " " +year, value: idx + 1 } })
  result = [{ name: "select...", value: -1 }].concat(result);
  months = result;
  return result;
}

export default class MonthSelector extends BaseComponent {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      value: props.value || -1
      ,year: props.year || -1
    };
  }

  handleChange(event) {
    let oldValue = this.state.value;
    let newValue = parseInt(event.target.value);
    if ( oldValue == newValue){
      return ;
    }
    this.setState({
      value: event.target.value
    });
    this.fireChange({
        target: this ,
        value:newValue,
        oldValue 
    });
  }

  fireChange(event){
    if ( this.props.onChange){
      this.props.onChange(event);
    }
  }

  get value() {
    return this.state.value;
  }
  
  get options() {
    return getMonths(this.state.year);
  }

  render() {
    const selectOptions = this.options.map((item, key) => <MonthOption key={key} data={item} />);
    let currentValue = this.state.value;
    return (
      <div className="col-sm-4 form-group  ">
        <label className="col-sm-2  control-label">Mese</label>
        <div className="col-sm-8" >
          <select className="col-sm-10 form-control" onChange={this.handleChange} value={this.state.value} >
            {selectOptions}
          </select>
        </div>
      </div>
    );
  }
}