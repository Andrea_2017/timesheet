import moment from 'moment' ;
import jquery from 'jquery' ;

import React from 'react';
import ReactDOM from 'react-dom';

const data = {}



export default class BaseClass {

    constructor() {


    }

    static get moment ()  {
        return moment;
    }

    static get jquery ()  {
        return jquery;
    }
    static get $ ()  {
        return this.jquery;
    }
    static get React ()  {
        return React;
    }
    static get ReactDOM ()  {
        return ReactDOM;
    }

}
