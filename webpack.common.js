var path = require('path');
var webpack = require("webpack");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var ManifestPlugin = require('webpack-manifest-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var AssetsPlugin = require('assets-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
module.exports = {
    entry: {
        main: './src/index.js',
        vendor: ['react', 'react-dom', 'moment', 'jquery'],
    },
    output: {
        filename: '[name].bundle.js',
        chunkFilename: '[name]-[chunkhash].js',
        path: path.resolve(__dirname, 'dist')
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
   
    // externals: {
    //     react: 'React'
    //     , 'react-dom': 'ReactDOM'
    //     , 'prop-types': 'PropTypes'
    // },
    module: {



        rules: [
            // Process any .js or .jsx file with Babel
            {
                test: /\.jsx?$/,
                exclude: [/node_modules/],
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,

                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader'],
                })

            },
            {
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                loader: 'url-loader',
                options: {
                    limit: 10000
                }
            },
            {
                test: /\.json$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]'
                            ,useRelativePath: true
                        }
                    }
                ]
            }
        ]

    },
    target: "web",
    plugins: [
        new ManifestPlugin(),
        new CleanWebpackPlugin(['dist']),
        new ExtractTextPlugin({ filename: 'app.bundle.css' }),
        new HtmlWebpackPlugin({
            hash: true
            , title: 'Synclab RIL application'
            , myPageHeader: 'Synclab RIL application'
            , template: './src/index.html'
            , filename: './index.html' //relative to root of the application
            , alwaysWriteToDisk: true
            , favicon: 'src/img/favicon/favicon.ico'
        }),
        new webpack.optimize.CommonsChunkPlugin({

            name: "vendor", filename: "vendor.bundle.js",
            name: "vendor", filename: "vendor.bundle.js",
        }),
        new AssetsPlugin({
            fullPath: true,
            prettyPrint: true
        }),
        new CopyWebpackPlugin([
            {from:'src/data',to:'data'} 
        ])
        //, new HtmlWebpackHarddiskPlugin()
    ]
};